from dataclasses import dataclass

from item import Item
@dataclass
class Basket(object):
    items:list[Item]

    def total(self):
        if len(self.items)==1:
            return self.items[0].unit_price
        elif len(self.items)==2:
            return (self.items[0].unit_price+self.items[1].unit_price)
        else:
            return 0